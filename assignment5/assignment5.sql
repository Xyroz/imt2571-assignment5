-- phpMyAdmin SQL Dump
-- version 4.7.2
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: 05. Nov, 2017 20:56 PM
-- Server-versjon: 10.1.26-MariaDB
-- PHP Version: 7.1.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `assignment5`
--

-- --------------------------------------------------------

--
-- Tabellstruktur for tabell `affiliations`
--

CREATE TABLE `affiliations` (
  `ID` varchar(250) COLLATE utf8_danish_ci NOT NULL,
  `username` varchar(250) COLLATE utf8_danish_ci NOT NULL,
  `fallyear` year(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_danish_ci;

-- --------------------------------------------------------

--
-- Tabellstruktur for tabell `clubs`
--

CREATE TABLE `clubs` (
  `ID` varchar(250) COLLATE utf8_danish_ci NOT NULL,
  `name` varchar(250) COLLATE utf8_danish_ci NOT NULL,
  `city` varchar(250) COLLATE utf8_danish_ci NOT NULL,
  `county` varchar(250) COLLATE utf8_danish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_danish_ci;

-- --------------------------------------------------------

--
-- Tabellstruktur for tabell `distances`
--

CREATE TABLE `distances` (
  `username` varchar(250) COLLATE utf8_danish_ci NOT NULL,
  `fallYear` varchar(250) COLLATE utf8_danish_ci NOT NULL,
  `distance` int(250) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_danish_ci;

-- --------------------------------------------------------

--
-- Tabellstruktur for tabell `logs`
--

CREATE TABLE `logs` (
  `username` varchar(250) COLLATE utf8_danish_ci NOT NULL,
  `date` varchar(250) COLLATE utf8_danish_ci NOT NULL,
  `area` varchar(250) COLLATE utf8_danish_ci NOT NULL,
  `distance` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_danish_ci;

-- --------------------------------------------------------

--
-- Tabellstruktur for tabell `skiers`
--

CREATE TABLE `skiers` (
  `username` varchar(250) COLLATE utf8_danish_ci NOT NULL,
  `firstname` varchar(100) COLLATE utf8_danish_ci NOT NULL,
  `lastname` varchar(100) COLLATE utf8_danish_ci NOT NULL,
  `year of birth` year(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_danish_ci;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `affiliations`
--
ALTER TABLE `affiliations`
  ADD PRIMARY KEY (`username`,`fallyear`),
  ADD KEY `ID` (`ID`);

--
-- Indexes for table `clubs`
--
ALTER TABLE `clubs`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `distances`
--
ALTER TABLE `distances`
  ADD PRIMARY KEY (`username`,`fallYear`);

--
-- Indexes for table `logs`
--
ALTER TABLE `logs`
  ADD PRIMARY KEY (`username`,`date`,`area`);

--
-- Indexes for table `skiers`
--
ALTER TABLE `skiers`
  ADD PRIMARY KEY (`username`);

--
-- Begrensninger for dumpede tabeller
--

--
-- Begrensninger for tabell `affiliations`
--
ALTER TABLE `affiliations`
  ADD CONSTRAINT `affiliations_ibfk_1` FOREIGN KEY (`username`) REFERENCES `skiers` (`username`) ON UPDATE CASCADE,
  ADD CONSTRAINT `affiliations_ibfk_2` FOREIGN KEY (`ID`) REFERENCES `clubs` (`ID`) ON UPDATE CASCADE;

--
-- Begrensninger for tabell `distances`
--
ALTER TABLE `distances`
  ADD CONSTRAINT `distances_ibfk_1` FOREIGN KEY (`username`) REFERENCES `skiers` (`username`) ON UPDATE CASCADE;

--
-- Begrensninger for tabell `logs`
--
ALTER TABLE `logs`
  ADD CONSTRAINT `logs_ibfk_1` FOREIGN KEY (`username`) REFERENCES `skiers` (`username`) ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
