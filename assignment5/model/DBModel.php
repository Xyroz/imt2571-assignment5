<?php
include_once("controller/Entities.php");

class DBModel 
{
	protected $db = null;

	public function __construct($db = null, $xmlFile = null)  
    {  
	    if ($db) 
		{
			$this->db = $db;
		}
		else
		{
            try
            {
            $this->db = new PDO('mysql:host=127.0.0.1;dbname=asd;charset=utf8', 
                                'admin', 'admin',  
                                 array(PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION));
            }
            catch (PDOException $e)
            {
                echo 'PDOException: ' . $e->getMessage(), PHP_EOL;
            }
		}
    }

    public function addClub($club) 
    {
    	$stmt = $this->db->prepare(
                        "INSERT INTO clubs 
                         VALUES (:id, :name, :city, :county);");
        
        $stmt->bindValue(':id',     $club->id);
        $stmt->bindValue(':name',   $club->name);
        $stmt->bindValue(':city',   $club->city);
        $stmt->bindValue(':county', $club->county);
        try
        {
            $stmt->execute();
        }
        catch (PDOException $e)
        {
            echo 'PDOException: AAAAAAA1 ' . $e->getMessage(), PHP_EOL;
        }
    }

    public function addSkier($skier) 
    {
        $stmt = $this->db->prepare(
                        "INSERT INTO skiers 
                         VALUES (:userName, :firstName, :lastName, :yearofbirth);");
        
        $stmt->bindValue(':userName',       $skier->username);
        $stmt->bindValue(':firstName',      $skier->firstname);
        $stmt->bindValue(':lastName',       $skier->lastname);
        $stmt->bindValue(':yearofbirth',    $skier->yearofbirth);
        try
        {
            $stmt->execute();
        }
        catch (PDOException $e)
        {
            echo 'PDOException: AAAAAAA2 ' . $e->getMessage(), PHP_EOL;
        }
    }

    public function addLog($log)
    {
        $stmt = $this->db->prepare(
                        "INSERT INTO logs 
                         VALUES (:userName, :adate, :area, :distance);");
        
        $stmt->bindValue(':userName',   $log->username);
        $stmt->bindValue(':adate',      $log->date);
        $stmt->bindValue(':area',       $log->area);
        $stmt->bindValue(':distance',   $log->distance);
        try
        {
            $stmt->execute();
        }
        catch (PDOException $e)
        {
            echo 'PDOException: AAAAAAA3 ' . $e->getMessage(), PHP_EOL;
        }
    }

    public function addAffiliation($affiliation)
    {
        if ($affiliation->id) 
        {
            $stmt = $this->db->prepare(
                            "INSERT INTO affiliations 
                             VALUES (:id, :username, :fallyear);");
            
            $stmt->bindValue(':id',             $affiliation->id);
            $stmt->bindValue(':username',       $affiliation->username);
            $stmt->bindValue(':fallyear',       $affiliation->fallyear);
            try
            {
                $stmt->execute();
            }
            catch (PDOException $e)
            {
                echo 'PDOException: AAAAAAA4 ' . $e->getMessage(), PHP_EOL;
            }
        }
    }
    public function calcDistance($dist)
    {
        $stmt = $this->db->prepare(
                        "INSERT INTO distances
                         VALUES (:username, :fallyear, :distance);");
        
        $stmt->bindValue(':username',       $dist->username);
        $stmt->bindValue(':fallyear',       $dist->fallyear);
        $stmt->bindValue(':distance',       $dist->distance);
        try
        {
            $stmt->execute();
        }
        catch (PDOException $e)
        {
            echo 'PDOException: AAAAAAA5 ' . $e->getMessage(), PHP_EOL;
        }

    }

}

?>