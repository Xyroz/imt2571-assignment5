<?php
include_once("model/DBModel.php");
include_once("controller/Entities.php");

class Controller 
{
	public $model;
	public $xmlFile;
    public $xpath;

	public function __construct()  
    {  
		session_start();
        $this->model = new DBModel();
        $this->xmlFile = new DOMDocument();
        $this->xmlFile->load("SkierLogs.xml");
        $this->xpath = new DOMXPath($this->xmlFile);
    } 

    public function invoke()
    {
        foreach ($this->xpath->query("//SkierLogs/Clubs/Club")  as $item) 
    	{
    		$club = new Club(
                $item->getAttribute('id'),
                $item->firstChild->nextSibling->textContent,
                $item->firstChild->nextSibling->nextSibling->nextSibling->textContent,
                $item->firstChild->nextSibling->nextSibling->nextSibling->nextSibling->nextSibling->textContent);

            $this->model->addClub($club);
    	} 

        foreach ($this->xpath->query("//SkierLogs/Skiers/Skier") as $item)
        {
            $skier = new Skier(
                $item->getAttribute('userName'),
                $item->firstChild->nextSibling->textContent,
                $item->firstChild->nextSibling->nextSibling->nextSibling->textContent,
                $item->firstChild->nextSibling->nextSibling->nextSibling->nextSibling->nextSibling->textContent);

            $this->model->addSkier($skier);
        }

        foreach ($this->xpath->query("//SkierLogs/Season/Skiers/Skier/Log/Entry") as $item)
        {
            $log = new Log(
                $item->parentNode->parentNode->getAttribute('userName'),
                $item->firstChild->nextSibling->textContent,
                $item->firstChild->nextSibling->nextSibling->nextSibling->textContent,
                $item->firstChild->nextSibling->nextSibling->nextSibling->nextSibling->nextSibling->textContent);

            $this->model->addLog($log);
        }

        foreach ($this->xpath->query("//SkierLogs/Season/Skiers/Skier") as $item)
        {
            $affil = new Affiliation(
                $item->parentNode->getAttribute('clubId'),
                $item->getAttribute('userName'),
                $item->parentNode->parentNode->getAttribute('fallYear'));

            $this->model->addAffiliation($affil);
        }


        foreach ($this->xpath->query("//SkierLogs/Season/Skiers/Skier") as $item)
        {
            $query = "sum(//SkierLogs/Season[@fallYear='" . $item->parentNode->parentNode->getAttribute('fallYear') . "']/Skiers/Skier[@userName='" . $item->getAttribute('userName') . "']/Log/Entry/Distance)";
            $distance = new Distance(
            $item->getAttribute('userName'),
            $item->parentNode->parentNode->getAttribute('fallYear'),
            $this->xpath->evaluate($query)
            );

            $this->model->calcDistance($distance);
        }

    }

}
?>